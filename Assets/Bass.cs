using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bass : MonoBehaviour
{

    AudioSource a;
    float loopStart = 3F;
    float loopEnd = 30F;

    // Start is called before the first frame update
    void Start()
    {
        // return;
        a = gameObject.AddComponent<AudioSource>();
        // playChord("F");
    }

    // Update is called once per frame
    void Update()
    {
        // return;
        // c.OnUpdate();
        if (a.isPlaying && a.time > loopEnd)
        {
            a.time = loopStart;
        }
    }

    public void playChord(string chord)
    {
        a.clip = Resources.Load<AudioClip>("chords/chord" + chord.ToUpper());
        a.time = 0;
        a.Play();
        a.spatialize = true;
        a.spatialBlend = 1;
    }

    public void stop()
    {
        a.Stop();
    }
}
