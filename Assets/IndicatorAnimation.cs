using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorAnimation : MonoBehaviour
{

    public float amplitude = 0.5f; // Height of the movement
    public float speed = 1.0f; // Speed of the movement

    private Vector3 startPosition;

    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        // Apply the new position
        transform.position = new Vector3(
            transform.position.x,
            startPosition.y + Mathf.Sin(Time.time * speed) * amplitude,
            transform.position.z
        );
    }
}
