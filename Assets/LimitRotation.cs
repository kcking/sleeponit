using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitRotation : MonoBehaviour
{
    Vector3? lastSavedPosition;
    Quaternion? lastSavedRotation;

    string KEY = "sleep_piano_transform";

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey(KEY))
        {
            string tStr = PlayerPrefs.GetString(KEY);
            DeserializeTransform(tStr, GetComponent<Transform>());
            lastSavedPosition = transform.position;
            lastSavedRotation = transform.rotation;
        }
    }

    void LateUpdate()
    {
        // Extract the current rotation around the Y-axis
        float yRotation = transform.eulerAngles.y;

        // Set the rotation while keeping the object flat (no rotation around X and Z axes)
        transform.rotation = Quaternion.Euler(0, yRotation, 0);

        if (
            (lastSavedPosition == null || lastSavedRotation == null)
            || (transform.position - lastSavedPosition.Value).magnitude > 0.001
            || (transform.rotation.eulerAngles - lastSavedRotation.Value.eulerAngles).magnitude > 0.001)
        {
            //  NEW TRANSFORM
            PlayerPrefs.SetString(KEY, SerializeTransform(transform));
        }
    }

    public static string SerializeTransform(Transform transform)
    {
        return transform.position.x + ":" + transform.position.y + ":" + transform.position.z + "|"
            + transform.rotation.x + ":" + transform.rotation.y + ":" + transform.rotation.z + ":" + transform.rotation.w + "|"
            + transform.localScale.x + ":" + transform.localScale.y + ":" + transform.localScale.z;
    }

    public static void DeserializeTransform(string serializedTransform, Transform transform)
    {
        string[] parts = serializedTransform.Split('|');

        // Deserialize position
        string[] positionString = parts[0].Split(':');
        Vector3 position = new Vector3(float.Parse(positionString[0]), float.Parse(positionString[1]), float.Parse(positionString[2]));
        transform.position = position;

        // Deserialize rotation
        string[] rotationString = parts[1].Split(':');
        Quaternion rotation = new Quaternion(float.Parse(rotationString[0]), float.Parse(rotationString[1]), float.Parse(rotationString[2]), float.Parse(rotationString[3]));
        transform.rotation = rotation;

        // Deserialize scale
        string[] scaleString = parts[2].Split(':');
        Vector3 scale = new Vector3(float.Parse(scaleString[0]), float.Parse(scaleString[1]), float.Parse(scaleString[2]));
        transform.localScale = scale;
    }
}
