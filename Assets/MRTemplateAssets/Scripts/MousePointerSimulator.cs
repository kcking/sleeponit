using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePointerSimulator : MonoBehaviour
{
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) // Check if the left mouse button is pressed
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null && hit.collider.gameObject.CompareTag("PointableObject"))
                {
                    // Call the method you want to trigger
                    hit.collider.gameObject.GetComponent<PointableObject>().OnPointerEnter();
                }
            }
        }
    }
}
