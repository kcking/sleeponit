using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointableObject : MonoBehaviour
{

    private AudioSource audioSource;

    void Start()
    {
        // Initialization code here (if needed)
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        // Regular update code here (if needed)
    }

    public void OnPointerEnter()
    {
        // Code for what happens when the object is pointed at
        Debug.Log("Object has been pointed at!");
        if (audioSource != null)
        {
            audioSource.Play();
        }
        else
        {
            Debug.LogError("No AudioSource found on the object.");
        }
    }

}
