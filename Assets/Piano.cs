using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Piano : MonoBehaviour
{
    // Chords
    static string[][] chords = new string[][]{
        new string[]{"C4", "E4", "G4"},
        new string[]{"F4", "A4", "C5"},
    };

    Dictionary<PianoKeyInteract, string[]> keyToChord = new Dictionary<PianoKeyInteract, string[]>();
    Dictionary<string, PianoKeyInteract> keyNameToObject = new Dictionary<string, PianoKeyInteract>();

    private string[] activeChord = null;
    private int numKeysPressed = 0;

    public GameObject indicator1;
    public GameObject indicator2;
    private float indicatorsTime = 2.0f;

    [SerializeField]
    private Vector3 indicatorOffset;

    // Start is called before the first frame update
    void Start()
    {
        mapChildrenAndChords();
    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void registerKeyClick(PianoKeyInteract key)
    {
        handleChords(key);
    }

    void handleChords(PianoKeyInteract key)
    {
        string name = key.GetComponent<AudioSource>().clip.name;

        // get chord that key is associated with
        string[] chord = keyToChord.GetValueOrDefault(key, null);

        int index = activeChord == null ? -1 : Array.IndexOf(activeChord, name);

        if (index >= 0)
        {
            if (index == 0 && numKeysPressed == 0)
            {
                numKeysPressed++;
            } else if (index == 1 && indicator1.active)
            {
                indicator1.gameObject.SetActive(false);
                numKeysPressed++;
            }
            else if (index == 2 && indicator2.active)
            {
                indicator2.gameObject.SetActive(false);
                numKeysPressed++;
            }

            if (numKeysPressed == 3)
            {
                // TODO: play theme
                Debug.Log("play theme");
            }
        }
        else if (chord != null && chord[0] == name)
        {
            // if key is not part of current active chord start the new chord
            showIndicatorsOnKeys(chord);

            Invoke("removeIndicatorsFromActiveChord", indicatorsTime); // Hide text after 2 seconds
        }
        else
        {
            // if key is not part of current active chord, close current chord
            removeIndicatorsFromActiveChord();
        }
    }

    private void showIndicatorsOnKeys(string[] chord)
    {
        // show indicators on other keys of the chord
        PianoKeyInteract chordKey = keyNameToObject.GetValueOrDefault(chord[1], null);
        moveIndicator(indicator1, chordKey);
        chordKey = keyNameToObject.GetValueOrDefault(chord[2], null);
        moveIndicator(indicator2, chordKey);

        activeChord = chord;
        numKeysPressed = 0;
    }

    private void removeIndicatorsFromActiveChord()
    {
        // remove indicators on all keys of current active chord
        indicator1.gameObject.SetActive(false);
        indicator2.gameObject.SetActive(false);

        activeChord = null;
        numKeysPressed = 0;
    }

    private void mapChildrenAndChords()
    {
        // map piano key names to their object PianoKeyInteract
        foreach (Transform child in transform)
        {
            PianoKeyInteract key = child.GetComponent<PianoKeyInteract>();
            if (key != null && !key.name.Contains("Black"))
            {
                keyNameToObject.Add(key.GetComponent<AudioSource>().clip.name, key);
            }
        }

        // map keys to chord that they start
        foreach (string[] chord in Piano.chords)
        {
            PianoKeyInteract key = keyNameToObject.GetValueOrDefault(chord[0], null);
            if (key != null)
            {
                keyToChord.Add(key, chord);
            }
        }
    }

    private void moveIndicator(GameObject indicator, PianoKeyInteract key)
    {
        indicator.gameObject.SetActive(true);
        Vector3 keyPosition = key.transform.position;
        Vector3 newIndicatorPosition = new Vector3(keyPosition.x, keyPosition.y, keyPosition.z);
        newIndicatorPosition -= indicatorOffset;
        indicator.transform.position = new Vector3(newIndicatorPosition.x, indicator.transform.position.y, newIndicatorPosition.z);
    }
}
