using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PianoKeyInteract : MonoBehaviour
{

    Vector3 originalPosition;
    [Header("Key Animation")]
    [SerializeField]
    Vector3 offset;

    // Timer for the animation of pressing a key
    float timer = 0f;
    // Timer for the select animation
    float selectAnimationTimer = 0f;
    // Timer to wait for deselecting a key after not recieving select key event
    float deselectTimer = 0f;

    [SerializeField]
    float timeToDown = 0.2f;
    [SerializeField]
    float timeDown = 0.1f;
    [SerializeField]
    float timeToUp = 0.2f;

    [Header("Key Select")]
    [SerializeField]
    Material baseMaterial;
    [SerializeField]
    Material selectMaterial;
    [SerializeField]
    float timeToHighlight = 0.5f;
    [SerializeField]
    float timeToDeselect = 0.2f;

    [Header("Debug Controls")]
    [SerializeField]
    bool debug = true;
    [SerializeField]
    KeyCode key;
    [SerializeField]
    KeyCode selectKey;

    bool playingAnimation = false;
    bool keySelected = false;

    private new Renderer renderer;
    private Color originalColor;
    public TextMeshProUGUI popupText;
    private AudioSource audioSource;

    private Piano piano;

    [SerializeField]
    private Vector3 textOffset;

    private bool justPressed = false;

    // Start is called before the first frame update
    void Start()
    {
        originalPosition = transform.position;
        renderer = GetComponent<Renderer>();
        originalColor = renderer.material.color;
        audioSource = GetComponent<AudioSource>();
        piano = GetComponentInParent<Piano>();
    }

    // Update is called once per frame
    void Update()
    {
        if (debug)
        {
            DebugControls();
        }

        if (playingAnimation)
        {
            PlayAnimation();
        }

        if (keySelected)
        {
            KeySelect();
            WaitForDeselect();

        }
        else
        {
            KeyDeselect();
        }
    }

    // Debug controls for playing animations outside MR
    void DebugControls()
    {
        if (Input.GetKeyDown(key))
        {
            PressPianoKey();
        }

        if (Input.GetKey(selectKey))
        {
            SelectPianoKey();
        }
    }


    // Triggers the audio and animation componet of a piano key being pressed.
    public void PressPianoKey()
    {
        playingAnimation = true;
        justPressed = true;
    }

    // Piano key selection event
    public void SelectPianoKey()
    {
        if (!keySelected)
        {
            selectAnimationTimer = 0f;
        }

        keySelected = true;
        deselectTimer = 0f;

    }

    // Plays the key press animation
    void HideText()
    {
        popupText.gameObject.SetActive(false);
    }

    void PlayAnimation()
    {
        if (piano != null)
        {
            piano.registerKeyClick(this);
        }

        if (audioSource != null && justPressed)
        {
            justPressed = false;
            audioSource.Play();
            if (popupText != null)
            {
                popupText.text = audioSource.clip.name;
                popupText.gameObject.SetActive(true);

                // Get current position of the piano key
                Vector3 keyPosition = transform.position;

                // Set a new x-value for the popupText position

                Vector3 newTextPosition = new Vector3(keyPosition.x, keyPosition.y, keyPosition.z);
                newTextPosition -= textOffset;

                popupText.transform.position = newTextPosition;
                Invoke("HideText", 2.0f); // Hide text after 2 seconds
            }
        }

        timer += Time.deltaTime;

        playingAnimation = true;

        if (timer < timeToDown)
        {
            renderer.material.color = new Color(0.53f, 0.81f, 0.92f, 0.3f);
            transform.position = Vector3.Lerp(originalPosition, originalPosition + offset, timer / timeToDown);
        }
        else if (timer > timeToDown + timeDown)
        {
            transform.position = Vector3.Lerp(originalPosition + offset, originalPosition, (timer - timeToDown - timeDown) / timeToUp);
        }

        if (timer > timeToDown + timeDown + timeToUp)
        {
            renderer.material.color = originalColor;
            playingAnimation = false;
            timer = 0f;
        }

    }

    // Animation for selecting a key
    void KeySelect()
    {
        selectAnimationTimer += Time.deltaTime;
        gameObject.GetComponent<Renderer>().material.Lerp(baseMaterial, selectMaterial, selectAnimationTimer / timeToHighlight);
    }

    // Waits for no more select events before deselecting the key
    void WaitForDeselect()
    {
        deselectTimer += Time.deltaTime;

        if (deselectTimer > timeToDeselect)
        {
            keySelected = false;
            selectAnimationTimer = 0f;
        }

    }

    // Animation for deselecting a key
    void KeyDeselect()
    {
        selectAnimationTimer += Time.deltaTime;

        if (Time.timeSinceLevelLoad < 0.5f)
        {
            return;
        }

        gameObject.GetComponent<Renderer>().material.Lerp(selectMaterial, baseMaterial, selectAnimationTimer / timeToHighlight);
    }

}
