using TMPro;
using UnityEngine;

public class JumpDetect : MonoBehaviour
{

    private float? downStart = null;
    private GameObject piano;
    [SerializeField] private EnviromentManager envManager;
    void Start()
    {
        piano = GameObject.Find("piano");
    }

    void Update()
    {
        castRayToHoverOverKey();

        var v = Camera.main.velocity;
        // Camera.main.transform.rotation 
        // var v = OVRManager.display.velocity;

        if (Vector3.Dot(v, Vector3.down) > 1)
        {
            downStart ??= Time.realtimeSinceStartup;
        }
        if (Vector3.Dot(v, Vector3.down) < 0.1 && downStart != null)
        {
            if (Time.realtimeSinceStartup - downStart > 0.05)
            {
                downStart = null;
                Debug.Log("JUMP");
                GameObject.Find("VelocityDebugText").GetComponent<TextMeshProUGUI>()?.SetText("" + v);
                OnJump();
                // Debug.Log(v.ToString());
            }
        }
        if (v != null)
        {
            // Debug.Log(v.ToString());
        }
        //GameObject.Find("VelocityDebugText").GetComponent<TextMeshProUGUI>()?.SetText(Vector3.Dot(v, Vector3.down).ToString()[..4]);
    }

    void castRayToHoverOverKey()
    {
        var t = Camera.main.transform;
        RaycastHit hit;
        var didHit = Physics.Raycast(t.position, Vector3.down, out hit);
        if (didHit && hit.collider.gameObject != null)
        {
            var inter = hit.collider.gameObject.GetComponent<PianoKeyInteract>();
            if (inter != null)
            {
                inter.SelectPianoKey();
            }
        }

    }

    void OnJump()
    {
        envManager.OnSuccess();

        var t = Camera.main.transform;
        RaycastHit hit;
        var didHit = Physics.Raycast(t.position, Vector3.down, out hit);
        if (didHit && hit.collider.gameObject != null)
        {
            // Find index of gameObject in Piano
            int? index = null;
            if (piano == null)
            {
                GameObject.Find("VelocityDebugText").GetComponent<TextMeshProUGUI>()?.SetText("piano null");
                return;
            }
            var parent = piano.transform;
            for (int i = 0; i < parent.childCount; i++)
            {
                if (parent.GetChild(i).gameObject.GetInstanceID() == hit.collider.gameObject.GetInstanceID())
                {
                    index = i;
                    break;
                }
            }
            if (index == null)
            {
                GameObject.Find("VelocityDebugText").GetComponent<TextMeshProUGUI>()?.SetText("Piano index not found");
                return;
            }
            GameObject.Find("VelocityDebugText").GetComponent<TextMeshProUGUI>()?.SetText("hit " + index);

            var networkLogic = GameObject.Find("NetworkLogic");
            if (!networkLogic)
            {
                GameObject.Find("VelocityDebugText").GetComponent<TextMeshProUGUI>()?.SetText("networkLogic NULL");
                return;
            }
            networkLogic.GetComponent<NetworkLogic>().PlayKeyRpc(index.Value);
            // hit.collider.gameObject.GetComponent<PointableObject>()?.OnPointerEnter();
            // hit.collider.gameObject.GetComponent<PianoKeyInteract>()?.PressPianoKey();
        }
        else
        {
            GameObject.Find("VelocityDebugText").GetComponent<TextMeshProUGUI>()?.SetText("didnt hit");
        }
    }
}
