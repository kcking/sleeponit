using System.Collections;
using System.Collections.Generic;
using Fusion;
using UnityEngine;

public class NetworkLogic : NetworkBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    [Rpc(RpcSources.All, RpcTargets.All)]
    public void PlayKeyRpc(int key)
    {
        GameObject.Find("piano").transform.GetChild(key).GetComponent<PianoKeyInteract>().PressPianoKey();
        GameObject.Find("piano").transform.GetChild(key).GetComponent<PointableObject>().OnPointerEnter();
        // hit.collider.gameObject.GetComponent<PointableObject>()?.OnPointerEnter();
        // hit.collider.gameObject.GetComponent<PianoKeyInteract>()?.PressPianoKey();
    }

    [Rpc(RpcSources.All, RpcTargets.All)]
    public void SpatialAnchorRPC() { }
}
