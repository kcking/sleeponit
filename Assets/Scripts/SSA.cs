using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using Meta.XR.BuildingBlocks;
using UnityEngine;

public class SSA : MonoBehaviour
{
    SpatialAnchorCoreBuildingBlock _core;
    string UUID_KEY = "sleepuuid";

    GameObject anchorPrefab;

    // Start is called before the first frame update
    void Start()
    {
        _core = FindObjectOfType<SpatialAnchorCoreBuildingBlock>();
        _core.OnAnchorCreateCompleted.AddListener(SaveAnchorUuid);
        _core.OnAnchorsLoadCompleted.AddListener(OnAnchorsLoaded);
    }

    public void CreateSpatialAnchor(Transform transform)
    {
        _core.InstantiateSpatialAnchor(anchorPrefab, transform.position, transform.rotation);
    }

    internal void SaveAnchorUuid(OVRSpatialAnchor anchor)
    {
        //  Require manual deletion so we dont accidentally erase.
        if (!PlayerPrefs.HasKey(UUID_KEY))
        {
            PlayerPrefs.SetString(UUID_KEY, anchor.Uuid.ToString());
        }
    }

    void LoadSpatialAnchor()
    {
        if (!PlayerPrefs.HasKey(UUID_KEY))
        {
            Debug.Log("no SSA uuid stored");
            return;
        }
        var uuid = PlayerPrefs.GetString(UUID_KEY);
        var guid = Guid.Parse(uuid);
        var uuids = new List<Guid> { guid };
        _core.LoadAndInstantiateAnchors(anchorPrefab, uuids);
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void OnAnchorsLoaded() { }
}
