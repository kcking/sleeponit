using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviromentObject : MonoBehaviour
{
    public EnviromentManager enviromentManager;
    [SerializeField] private int note=-1; //the musical note associated with this object 
    [SerializeField] private int stage; //for objects that have multiple elements to their activate [e.g. a planet rotating, orbiting, etf]
    [SerializeField] private int maxStage;
    [SerializeField] protected int minActivationStage; // to stagger objects behaviour


    // Start is called before the first frame update
    void Start()
    {
        if(enviromentManager == null){enviromentManager = FindObjectOfType<EnviromentManager>();}

        if(note != -1 && enviromentManager != null)
        {
            enviromentManager.OnNotePlayed += OnNotePlayed;

        }
    }

    // Update is called once per frame
    void Update()
    {
        Act();
    }

    public virtual void Act()
    {}

    public void Activate()
    {
        stage = Mathf.Clamp(stage + 1,0,maxStage);
        OnActivate();
    }


    public void DeActivate()
    {
         OnDeActivate();
        stage = Mathf.Clamp(stage - 1,0,maxStage);
       
    }

    public virtual void OnActivate()
    {
        
    }

    public virtual void OnDeActivate()
    {
        
    }

    public void OnNotePlayed(int _note)
    {
        if(_note == Note())
        {
            Activate();
        }
    }

    public int Note()
    {
        return note;
    }
    public int Stage()
    {
        return stage;
    }


}
