using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorShower : EnviromentObject
{
    public ParticleSystem particle;



    public override void OnActivate()
    {
        if(Stage() < minActivationStage){return;}
        if(particle == null){return;}

        particle.Play();
    }

    public override void OnDeActivate()
    {

    }


}
