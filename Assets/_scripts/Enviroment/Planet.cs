using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : EnviromentObject
{
    public Transform planetCenter;
    public Transform body;

    public float orbitSpeed;
    public float rotationSpeed;
    public float turnToFaceSpeed;
    public Transform patrolparent;
    public Transform target;
    public int patrolCount;

    public override void Act()
    {
        if(Stage() <= 0){return;}

        Orbit();


    }

    public void Orbit()
    {
        if(body != null)
        {
            body.Rotate(planetCenter.up * rotationSpeed * Time.deltaTime);
        }

        if(target == null){return;}

        var lookPos = target.position - planetCenter.position;
        var rotation = Quaternion.LookRotation(lookPos);
        planetCenter.rotation = Quaternion.Slerp(planetCenter.rotation, rotation, Time.deltaTime * turnToFaceSpeed);

        planetCenter.position += planetCenter.forward * orbitSpeed * Time.deltaTime;

        if(Vector3.Distance(planetCenter.position,target.position) < 1)
        {
            GetNextPatrolPoint();

        }

    }

    public void GetNextPatrolPoint()
    {
        patrolCount++;
        if(patrolCount >= patrolparent.childCount){patrolCount = 0;}

        target = patrolparent.GetChild(patrolCount);

    }

    public override void OnActivate()
    {
        if(Stage() < minActivationStage){return;}
        body?.gameObject.SetActive(true);
    }

    public override void OnDeActivate()
    {
        body?.gameObject.SetActive(false);
    }
}
