using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Starbelt : EnviromentObject
{
    public ParticleSystem particle;
    public int maxParticlesPerStage = 3;


    public override void OnActivate()
    {

        if(Stage() < minActivationStage){return;}
        if(particle == null){return;}

        if(Stage() == 1)
        {
           

        }else  if(Stage() == 2)
        {
            
            var trails = particle.trails;
            trails.ratio = 1;
        }

        var main = particle.main;
        main.maxParticles = Mathf.RoundToInt(Stage() * maxParticlesPerStage);

        particle.Play();


    }

    public override void OnDeActivate()
    {
        if(particle == null){return;}

        var main = particle.main;
        main.maxParticles = Mathf.RoundToInt(Stage() * maxParticlesPerStage);

        if(Stage() == 1)
        {
           particle.Stop();

        }else  if(Stage() == 2)
        {
            
            var trails = particle.trails;
            trails.ratio = 0;
        }

        
    }

}
