using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class EnviromentManager : MonoBehaviour
{

    [SerializeField]private List<EnviromentObject> sequentialActivation;
    [SerializeField]private int activationStage; 

    public event Action<int> OnNotePlayed; 


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        DebugControls();
    }

    public void DebugControls()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            NotePlayed(0);
        }

        if(Input.GetKeyDown(KeyCode.S))
        {
             NotePlayed(1);
        }
        if(Input.GetKeyDown(KeyCode.D))
        {
             NotePlayed(2);
        }

        if(Input.GetKeyDown(KeyCode.Q))
        {
             OnSuccess();
        }
        if(Input.GetKeyDown(KeyCode.D))
        {
             OnFail();
        }

    }

    public void OnSuccess( )
    {
        if(sequentialActivation == null || activationStage >= sequentialActivation.Count){return;}

        activationStage = Mathf.Clamp(activationStage + 1,0,sequentialActivation.Count);

        foreach(EnviromentObject el in sequentialActivation)
        {
            el.Activate();

        }
       // sequentialActivation[activationStage].Activate();
    }

    public void OnFail( )
    {
        if(sequentialActivation == null || activationStage == 0){return;}

        sequentialActivation[activationStage].DeActivate();
        foreach(EnviromentObject el in sequentialActivation)
        {
            el.Activate();

        }

        activationStage = Mathf.Clamp(activationStage - 1,0,sequentialActivation.Count);
        
    }

    public void NotePlayed(int _note)
    {
        //if we want to activate objects independant of succesful chords
        //one time flourish? meteor shower for a moment etc
        OnNotePlayed.Invoke(_note);
    }

}
