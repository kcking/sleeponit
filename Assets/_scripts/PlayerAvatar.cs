using Fusion;
using UnityEngine;
using System.Collections.Generic;

public class PlayerAvatar : NetworkBehaviour
{
    [SerializeField]private MeshRenderer rend;
    [SerializeField]private List<Material> colorList;

    [SerializeField]private NetworkObject networkObject;

    [Networked] public Color NetworkedColor { get; set; }
    [Networked] public PlayerRef playerRef { get; set; }

    [Networked] public Vector3 NetworkedPosition { get; set; }
    [Networked] public Vector3 NetworkedRotation { get; set; }
    public float networkSpeed = 3;

    private Vector3 targetPosition;
    private Vector3 targetRotation;

    private ChangeDetector _changeDetector;

    public override void Spawned()
	{
		base.Spawned();
        networkObject = GetComponent<NetworkObject>();
        if(networkObject)
        {
            Debug.Log(GetComponent<NetworkObject>().InputAuthority);
            NetworkedColor = GetColor(GetComponent<NetworkObject>().InputAuthority.PlayerId).color;
            rend.material.color = GetColor(GetComponent<NetworkObject>().InputAuthority.PlayerId).color;

        }
        _changeDetector = GetChangeDetector(ChangeDetector.Source.SimulationState);

	}


    void Start()
    {
        
    }

   
    void Update()
    {
        foreach (var change in _changeDetector.DetectChanges(this))
        {
            switch (change)
            {
                case nameof(NetworkedColor):
                    rend.material.color = NetworkedColor;
                    break;
                case nameof(NetworkedPosition):
                    targetPosition = NetworkedPosition;
                    break;
                case nameof(NetworkedRotation):
                    targetRotation = NetworkedRotation;
                    break;
            }
        }

        SyncToPosition();

    }

    public void SyncToPosition()
    {
        Vector3 vec = (targetPosition - transform.position).normalized;

        if(Vector3.Distance(transform.position,targetPosition) < (vec * Time.deltaTime * networkSpeed).magnitude){transform.position = targetPosition;}
        if(Vector3.Distance(transform.eulerAngles,targetRotation) < 0.5f){transform.eulerAngles = targetRotation;}

       
        transform.position += vec * Time.deltaTime * networkSpeed;
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles,targetRotation,Time.deltaTime * networkSpeed);

    }

    public void SetColor(Material _mat)
    {
        if(rend == null){return;}

        rend.material = _mat;
    }

    public Material GetColor(int _value)
    {
        if(colorList == null)
        {
            colorList = new List<Material>();
        }

        return colorList[(int)Mathf.Clamp(_value,0,colorList.Count)];
    }

}
