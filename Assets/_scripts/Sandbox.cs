using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Fusion;
using System.Linq;

public class Sandbox : NetworkBehaviour
{
    public NetworkRunner networkRunner;
    public TMP_Text text;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowPlayerCount()
    {
        if(networkRunner == null){return;}
        if(text == null) {return;}

        text.SetText(Runner.ActivePlayers.Count().ToString());
    }


}
