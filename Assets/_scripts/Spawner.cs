using Fusion;
using UnityEngine;
using System.Collections.Generic;

public class Spawner : SimulationBehaviour, IPlayerJoined
{
    public GameObject PlayerPrefab;
    public List<Material> colorList;


    public void PlayerJoined(PlayerRef player)
    {
        Runner.Spawn(PlayerPrefab, new Vector3(0, 1, 0), Quaternion.identity, player);
        if (player == Runner.LocalPlayer)
        {
            
        }


    }

    public Material GetColor(int _value)
    {
        if(colorList == null)
        {
            colorList = new List<Material>();
        }

        return colorList[(int)Mathf.Clamp(_value,0,colorList.Count)];
    }
}